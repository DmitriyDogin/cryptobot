import datetime
import matplotlib.pyplot as plt
#import matplotlib.dates as mdates
import os
import random
import re
import requests
import sqlite3
import time

from collections import OrderedDict
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException

class XPath:
    def __init__(self, path, strip):
        self.path = path
        self.strip = strip

    def getPath (self, index):
        left_path = self.path[:self.strip-1]
        right_path = self.path[self.strip:]
        return left_path + str (index) + right_path

class Pair:
    def __init__ (self, time, last_cost, name="", volume_24=0, change_24=0, max=0, min=0, cursor=0):
        self.volume_24 = volume_24
        self.costs = OrderedDict()
        self.costs[time] = last_cost
        self.change_24 = change_24
        self.max = max
        self.min = min
        if cursor != 0:
            self.create_table (name, time, last_cost, cursor)

    def create_table (self, name, time, last_cost, cursor):
        query_create = "CREATE TABLE IF NOT EXISTS '{}' (time timestamp PRIMARY KEY, value REAL NOT NULL)".format (name)
        cursor.execute (query_create)
        query_insert = "INSERT INTO '{}' VALUES(?, ?)".format (name)
        cursor.execute (query_insert, (time, last_cost))

    def update (self, time, last_cost, name="", volume_24=0, change_24=0, max=0, min=0, cursor=0):
        self.volume_24 = volume_24
        date_cost = list(self.costs)[-1]
        if self.costs [date_cost] != last_cost:
            self.costs[time] = last_cost
            if cursor != 0:
                print ("cost is changed for ", name, "| cost =", self.costs [date_cost], "date =", date_cost, "last cost =", last_cost)
                self.update_table (name, time, last_cost, cursor)    
        self.change_24 = change_24
        self.max = max
        self.min = min

    def update_table (self, name, time, last_cost, cursor):
        query_insert = "INSERT INTO '{}' VALUES(?, ?)".format (name)
        cursor.execute (query_insert, (time, last_cost))

    def get_change (self, duration):
        min_date = datetime.datetime.now() - datetime.timedelta (seconds=duration)
        print (min_date)
        dates = list (self.costs)
        reversed_date = list (reversed (dates))
        beg_date = reversed_date[0]
        end_date = reversed_date[0]
        for i in range (1, len (reversed_date)):
            beg_date = reversed_date[i-1]
            if reversed_date[i] < min_date:
                print (min_date, reversed_date[i-1], reversed_date[i])
                break
        print (beg_date, end_date)
        beg_cost = self.costs [beg_date]
        end_cost = self.costs [end_date]
        res = (end_cost - beg_cost) / beg_cost * 100
        print (res, "=", end_cost, "-", beg_cost)
        return res

    def plot_rates (self, name):
        keys = list (self.costs)
        values = []
        for key in keys:
            values.append (self.costs [key])
        fig, ax = plt.subplots()
        print (name)
        ax.plot (keys, values, label=name)
        fig.autofmt_xdate()
        plt.legend (loc="best")
        plt.show()


class LiveCoinAnalyzer:
    def __init__ (self):
        #self.driver = webdriver.Firefox (executable_path="C:/cygwin64/home/ddoginPC/geckodriver/geckodriver.exe")
        self.driver = webdriver.Chrome (executable_path='/home/' + os.getlogin() + '/chromedriver/chromedriver')
        self.main_page = "https://www.livecoin.net/ru"
        self.pairs = {}

        self.name      = XPath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[2]/div/div[2]/div/div[1]/div[1]/div', 86)
        self.volume_24 = XPath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[2]/div/div[2]/div/div[1]/div[2]/span', 86)
        self.last_cost = XPath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[2]/div/div[2]/div/div[1]/div[3]/span', 86)
        self.change_24 = XPath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[2]/div/div[2]/div/div[1]/div[4]/span/span', 86)
        self.max       = XPath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[2]/div/div[2]/div/div[1]/div[5]/span', 86)
        self.min       = XPath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[2]/div/div[2]/div/div[1]/div[6]/span', 86)

        self.conn = sqlite3.connect ("mydb.db", detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        self.cursor = self.conn.cursor()

        self.read_from_db()

    def read_from_db (self):
        query_select_names = "SELECT NAME FROM sqlite_master WHERE type = 'table'"
        self.cursor.execute (query_select_names)
        names = self.cursor.fetchall()
        for name in names:
            name = name [0]
            query_select_pairs = "SELECT * FROM '{}'".format (name)
            self.cursor.execute (query_select_pairs)
            pairs = self.cursor.fetchall()
            for pair in pairs:
                if self.pairs.get (name, 0) == 0:
                    self.pairs[name] = Pair (pair[0], pair[1])
                else:
                    self.pairs[name].update (pair[0], pair[1])

    def analize (self, duration=120):
        self.driver.get (self.main_page)
        valute_usd = self.driver.find_element_by_xpath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[1]/div[4]')
        valute_usd.click()
        sort_by_volume = self.driver.find_element_by_xpath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[2]/div/div[1]/div[2]/span[2]')
        sort_by_volume.click()
        i = 0
        scrollTo = 0
        lastScrollTo = 0
        startTime = datetime.datetime.now()
        while True:
            try:
                i+=1
                name = self.driver.find_element_by_xpath (self.name.getPath (i))
                scrollTo = name.location['y']
                name = name.text
                volume_24 = self.string_to_double (self.driver.find_element_by_xpath (self.volume_24.getPath (i)).text)
                last_cost = self.string_to_double (self.driver.find_element_by_xpath (self.last_cost.getPath (i)).text)
                if last_cost == 0.:
                    continue
                change_24 = self.string_to_double (self.driver.find_element_by_xpath (self.change_24.getPath (i)).text)
                max       = self.string_to_double (self.driver.find_element_by_xpath (self.max.getPath (i)).text)      
                min       = self.string_to_double (self.driver.find_element_by_xpath (self.min.getPath (i)).text)
                cur_time = datetime.datetime.now()
                if self.pairs.get (name, "") == "":
                    print ("add new pair")
                    self.pairs[name] = Pair (cur_time, last_cost, name, volume_24, change_24, max, min, self.cursor)
                else:
                    self.pairs[name].update (cur_time, last_cost, name, volume_24, change_24, max, min, self.cursor)
            except NoSuchElementException:
                self.conn.commit()
                i = 1
                currentTime = datetime.datetime.now()
                timeDelta = currentTime - startTime
                if timeDelta.seconds > duration:
                    print ("time is end")
                    break
            except StaleElementReferenceException:
                i-=1
            except requests.packages.urllib3.exceptions.MaxRetryError:
                print ("strange exception")
                self.driver.quit()
                self.driver = webdriver.Chrome (executable_path='/home/' + os.getlogin() + '/chromedriver/chromedriver')
                self.driver.get (self.main_page)
                valute_usd = self.driver.find_element_by_xpath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[1]/div[4]')
                valute_usd.click()
                sort_by_volume = self.driver.find_element_by_xpath ('//*[@id="marketOverviewContainer"]/section/div/article/div/div[2]/div/div[1]/div[4]/span[2]')
                sort_by_volume.click()
        
                i = 0
                scrollTo = 0
                lastScrollTo = 0
                continue
        
    def get_better_pair_scalping (self, duration=1800):
        pairs_large_change_24 = []
        for pair in self.pairs:
            if self.pairs [pair].change_24 >= 0.0 and self.pairs [pair].volume_24 >= 0.0:
                pairs_large_change_24.append (pair)
        if len (pairs_large_change_24) == 0:
            print ("none pairs")
            return None
        best_pair = self.pairs [pairs_large_change_24[0]]
        best_pair_name = ""
        best_change = 0
        for pair in pairs_large_change_24:
            print (pair)
            change = self.pairs [pair].get_change (duration)
            if change > best_change and change < 6:
                best_change = change
                best_pair = self.pairs [pair]
                best_pair_name = pair
        print ("bests change =", best_change)
        return best_pair_name, best_pair

    def string_to_double (self, string):
        val = re.findall (r"[-+]?\d*\.\d+|\d+", string)[0]
        res = float (val)
        return res

    def printInfo (self):
        print ("print infoo")
        for key in self.pairs:
            try:
                print (key, self.pairs[key].volume_24, self.pairs[key].costs, self.pairs[key].change_24, self.pairs[key].max, self.pairs[key].min)
            except StaleElementReferenceException:
                print ("error printing element")
            print('\n')