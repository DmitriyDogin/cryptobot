import json
import hashlib
import hmac
import requests
import urllib

class LiveCoinBot:
    def __init__ (self):
        self.pin = "5698"
        self.url = "https://api.livecoin.net"
        self.api_key = "GbhzY2Nk9uDhQ6m267qNxZP2QREAuuHZ"
        self.secret_key = b"pHq3JtWGcGebnWJzaQfcUzBmGfVvCpWY"

    def sha256(self, data):
        H = hmac.new(key = self.secret_key, digestmod=hashlib.sha256)
        H.update(data.encode('utf-8'))
        return H.hexdigest().upper()

    def GetBalancesInfo (self, currency='USD'):
        method = "/payment/balance"
        adress = self.url + method
        params = {'currency': currency}
        encoding_params = urllib.parse.urlencode (params)
        encoding_params = encoding_params.encode ('utf-8')
        sign = hmac.new(self.secret_key, msg=encoding_params, digestmod=hashlib.sha256).hexdigest().upper()
        headers = {"Api-key": self.api_key, "Sign": sign, "Content-type": "application/x-www-form-urlencoded"}
        resp = requests.get (adress, headers=headers, params=params)
        print (resp.status_code)
        print (resp.text)

    def GetComissionInfo (self):
        method = '/exchange/commission'
        adress = self.url + method
        sign = hmac.new(self.secret_key, digestmod=hashlib.sha256).hexdigest().upper()
        headers = {"Api-key": self.api_key, "Sign": sign, "Content-type": "application/x-www-form-urlencoded"}
        resp = requests.get (adress, headers=headers)
        resp_data = resp.json()
        print (resp.status_code)
        print (resp_data['fee'])
