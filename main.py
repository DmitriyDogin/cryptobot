import sys

from botv3 import LiveCoinBot
from exchangeAnalyzer import LiveCoinAnalyzer

def main (argv):
    if len (argv) == 1 or argv[1] == '-a': #analize
        print ('analize is started  ')
        analyzer = LiveCoinAnalyzer()
        analyzer.read_from_db()
        analyzer.analize (300) #10 minutes
        analyzer.printInfo()
        name, best_pair = analyzer.get_better_pair_scalping (3600)
        best_pair.plot_rates (name)
    elif argv[1] == '-t': #trade    
        print ('livecoin bot is started')
        bot = LiveCoinBot()
        bot.GetBalancesInfo ('BTC')
        bot.GetComissionInfo()
    else:   
        print ("unknown parametr -", argv[1])

if __name__ == "__main__":
    main (sys.argv)